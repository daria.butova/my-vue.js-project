import { action } from '@storybook/addon-actions'

import Search from '../components/Search.vue'

export default {
  component: [Search],
  title: 'Search'
}

export const SearchBlock = () => ({
  components: { Search },
  template: '<Search></Search>',
  methods: { action: action('clicked') }
})
