import { action } from '@storybook/addon-actions'

import TopHeader from '../components/TopHeader.vue'

export default {
  component: [TopHeader],
  title: 'TopHeader'
}

export const TopHeaderBlock = () => ({
  components: { TopHeader },
  template: '<TopHeader></TopHeader>',
  methods: { action: action('clicked') }
})