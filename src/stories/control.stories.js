import Control from "../components/ControlPanel.vue";

export default {
  component: Control,
  title: "Control"
};

const optionItems = [
  {
    id: 0,
    title: "Release Date",
    checked: true
  },
  {
    id: 1,
    title: "Rating",
    checked: false
  }
];

export const withTextAndSwitcher = () => ({
  components: { Control },
  template: "<Control :title='title' :count='count'  :options='options'/>",
  props: {
    title: {
      default: () => "Sort By"
    },
    count: {
      default: () => 5
    },
    options: {
      default: () => optionItems
    }
  }
});
