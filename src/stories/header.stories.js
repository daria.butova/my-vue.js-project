import { action } from '@storybook/addon-actions'

import Header from '../components/Header.vue'

export default {
  component: [Header],
  title: 'Header'
}

export const HeaderBlock = () => ({
  components: { Header },
  template: '<Header></Header>',
  methods: { action: action('clicked') }
})