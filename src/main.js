import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

import InViewDirective from './plugins/directive'

Vue.use(InViewDirective);

new Vue({
  router,
  store,
  render: h => h(App),
  created() {
    this.$store.dispatch('populateMovies');
  }
}).$mount('#app')
